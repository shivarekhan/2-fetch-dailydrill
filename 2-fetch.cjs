
/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/
//Fetch all the users
function fetchAllUser(userUrl) {
    fetch(userUrl)
        .then(function (userUrlResponse) {

            if (!userUrlResponse.ok) {
                throw new Error("resopnse not recived");
            } else {
                return userUrlResponse.json();
            }

        })
        .then(function (userUrlResponse) {

            console.log(userUrlResponse);

        })
        .catch(function (error) {
            console.log(error);
        });
}

//Fetch all the todos
function fetchAllToDos(todosUrl) {
    fetch(todosUrl)
        .then(function (todosUrlResponse) {

            if (!todosUrlResponse.ok) {
                throw new Error("response not recived");
            } else {
                return todosUrlResponse.json();
            }

        })
        .then(function (todosUrlResponse) {

            console.log(todosUrlResponse);

        })
        .catch(function (error) {
            console.log(error);
        });
}

//Use the promise chain and fetch the users first and then the todos.
function userFirstThenTodos(userUrl, todosUrl) {
    fetch(userUrl)
        .then(function (userUrlResponse) {

            if (!userUrlResponse.ok) {
                throw new Error("Response not recived for users");
            } else {
                console.log("response recived for user data");
                return fetch(todosUrl);
            }

        })
        .then(function (todosUrlResponse) {

            if (!todosUrlResponse.ok) {
                throw new Error("resopnse not recived for todos");
            }
            else {
                console.log("Response for the todos recived");
            }

        })
        .catch(function (error) {
            console.log(error);
        });
}


//Use the promise chain and fetch the users first and then all the details for each user.

function fetchUserFirstThenAllDetailsForEachUser(userUrl) {

    function usersDetailsByID(id) {
        return new Promise(function (resolve, reject) {

            if (typeof id !== 'number' || id === 0 || id === undefined) {
                const error = new Error("id is invalid");
                reject(error);
            } else {
                const urlForSpecficId = userUrl + `?id=${id}`;
                console.log(urlForSpecficId);
                fetch(urlForSpecficId)
                    .then(function (responseForSpecficUser) {

                        if (!responseForSpecficUser.ok) {
                            const error = new Error(`Data not fetched for the id = ${id}`);
                            reject(error);
                        } else {
                            // console.log(responseForSpecficUser.json());
                            return responseForSpecficUser.json();
                        }

                    })
                    .then(function (responseForSpecficUser) {
                        resolve(responseForSpecficUser);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        });
    }

    fetch(userUrl)
        .then(function (userUrlResponse) {

            if (!userUrlResponse.ok) {
                throw new Error("Response for the userUrl not recived");
            } else {
                return userUrlResponse.json();
            }

        })
        .then(function (userUrlResponse) {
            
            console.log(userUrlResponse.id);
            const userIds = Object.entries(userUrlResponse).map(function (currentUser) {
                console.log(currentUser[1].id);
                return currentUser[1].id;
            });

            const userData = userIds.map(function (currentId) {
                console.log(currentId);
                return usersDetailsByID(currentId);
            })
            return Promise.all(userData);

        })
        .then(function (userResponse) {
            console.log(userResponse);
        })
        .catch(function (error) {
            console.log(error);
        });

}

